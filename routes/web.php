<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('modules.home.index');
});

Auth::routes();
Route::get('/', function () {
    return view('home');
});
Route::get('/home-ad', 'HomeController@index')->name('home');
