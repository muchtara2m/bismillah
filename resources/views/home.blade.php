<!doctype html>
<html lang="en">
<head>
    
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <title>Protfolio | Ayro UI</title>
    
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/png">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/LineIcons.css') }}">
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('css/default.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
</head>
<body>
    
    <section class="page-banner mb-100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="page-banner-content text-center">
                        <h1 class="sub-title">AYRO UI - STARTUP AND SAAS BUSINESS UI KIT</h1>
                        <h3 class="page-title">Portfolio Styles</h3>
                    </div> 
                </div>
            </div> 
        </div> 
    </section>
    
    
    <section class="portfolio-area portfolio-one pb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portfolio-menu">
                        <ul>
                            <li data-filter="*" class="active">ALL WORK</li>
                            <li data-filter=".branding">BRANDING</li>
                            <li data-filter=".marketing">MARKETING</li>
                            <li data-filter=".planning">PLANNING</li>
                            <li data-filter=".research">RESEARCH</li>
                        </ul>
                    </div> 
                </div>
            </div> 
            <div class="row grid">
                <div class="col-lg-4 col-sm-6 branding planning">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-1/1.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup-two" href="{{ asset('images/portfolio/portfolio-1/1.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-1/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 marketing research">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-1/2.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup-two" href="{{ asset('images/portfolio/portfolio-1/2.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-1/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 branding marketing">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-1/3.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup-two" href="{{ asset('images/portfolio/portfolio-1/3.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-1/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 planning research">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-1/4.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup-two" href="{{ asset('images/portfolio/portfolio-1/4.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-1/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 marketing">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-1/5.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup-two" href="{{ asset('images/portfolio/portfolio-1/5.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-1/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 planning">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-1/6.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup-two" href="{{ asset('images/portfolio/portfolio-1/6.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-1/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 research">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-1/7.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup-two" href="{{ asset('images/portfolio/portfolio-1/6.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-1/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 branding planning">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-1/8.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup-two" href="{{ asset('images/portfolio/portfolio-1/8.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-1/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 marketing">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-1/9.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup-two" href="{{ asset('images/portfolio/portfolio-1/9.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-1/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div> 
        </div> 
    </section>
    
    
    <section class="portfolio-area portfolio-two pb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portfolio-menu-2 text-center">
                        <ul>
                            <li data-filter="*" class="active">ALL WORK</li>
                            <li data-filter=".branding-2">BRANDING</li>
                            <li data-filter=".marketing-2">MARKETING</li>
                            <li data-filter=".planning-2">PLANNING</li>
                            <li data-filter=".research-2">RESEARCH</li>
                        </ul>
                    </div> 
                </div>
            </div> 
            <div class="row grid-2">
                <div class="col-lg-4 col-sm-6 branding-2 planning-2">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-2/1.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup" href="{{ asset('images/portfolio/portfolio-2/1.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-icon">
                                    <a href="#"><i class="lni-link"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 marketing-2 research-2">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-2/2.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup" href="{{ asset('images/portfolio/portfolio-2/2.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-icon">
                                    <a href="#"><i class="lni-link"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 branding-2 marketing-2">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-2/3.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup" href="{{ asset('images/portfolio/portfolio-2/3.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-icon">
                                    <a href="#"><i class="lni-link"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 planning-2 research-2">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-2/4.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup" href="{{ asset('images/portfolio/portfolio-2/4.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-icon">
                                    <a href="#"><i class="lni-link"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 marketing-2">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-2/5.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup" href="{{ asset('images/portfolio/portfolio-2/5.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-icon">
                                    <a href="#"><i class="lni-link"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 planning-2">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-2/6.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup" href="{{ asset('images/portfolio/portfolio-2/6.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-icon">
                                    <a href="#"><i class="lni-link"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 research-2">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-2/7.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup" href="{{ asset('images/portfolio/portfolio-2/7.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-icon">
                                    <a href="#"><i class="lni-link"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 branding-2 planning-2">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-2/8.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup" href="{{ asset('images/portfolio/portfolio-2/8.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-icon">
                                    <a href="#"><i class="lni-link"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 marketing-2">
                    <div class="single-portfolio mt-30 text-center">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-2/9.png') }}" alt="">
                        </div>
                        <div class="portfolio-overlay d-flex align-items-center">
                            <div class="portfolio-content">
                                <div class="portfolio-icon">
                                    <a class="image-popup" href="{{ asset('images/portfolio/portfolio-2/9.png') }}"><i class="lni-zoom-in"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-icon">
                                    <a href="#"><i class="lni-link"></i></a>
                                    <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                </div>
                                <div class="portfolio-text">
                                    <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                                    <p class="text">Short description for the ones who look for something new. Awesome!</p>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div> 
        </div> 
    </section>
    
    
    <section class="portfolio-area portfolio-three pb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="portfolio-menu-3 text-center">
                        <ul>
                            <li data-filter="*" class="active">ALL WORK</li>
                            <li data-filter=".branding-3">BRANDING</li>
                            <li data-filter=".marketing-3">MARKETING</li>
                            <li data-filter=".planning-3">PLANNING</li>
                            <li data-filter=".research-3">RESEARCH</li>
                        </ul>
                    </div> 
                </div>
            </div> 
            <div class="row grid-3">
                <div class="col-lg-4 col-sm-6 branding-3 planning-3">
                    <div class="single-portfolio mt-30">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-3/1.png') }}" alt="">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup-three" href="{{ asset('images/portfolio/portfolio-3/1.png') }}"><i class="lni-zoom-in"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                    <div class="portfolio-icon">
                                        <a href="#"><i class="lni-link"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 marketing-3 research-3">
                    <div class="single-portfolio mt-30">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-3/2.png') }}" alt="">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup-three" href="{{ asset('images/portfolio/portfolio-3/2.png') }}"><i class="lni-zoom-in"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                    <div class="portfolio-icon">
                                        <a href="#"><i class="lni-link"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 branding-3 marketing-3">
                    <div class="single-portfolio mt-30">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-3/3.png') }}" alt="">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup-three" href="{{ asset('images/portfolio/portfolio-3/3.png') }}"><i class="lni-zoom-in"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                    <div class="portfolio-icon">
                                        <a href="#"><i class="lni-link"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 planning-3 research-3">
                    <div class="single-portfolio mt-30">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-3/4.png') }}" alt="">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup-three" href="{{ asset('images/portfolio/portfolio-3/4.png') }}"><i class="lni-zoom-in"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                    <div class="portfolio-icon">
                                        <a href="#"><i class="lni-link"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 marketing-3">
                    <div class="single-portfolio mt-30">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-3/5.png') }}" alt="">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup-three" href="{{ asset('images/portfolio/portfolio-3/5.png') }}"><i class="lni-zoom-in"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                    <div class="portfolio-icon">
                                        <a href="#"><i class="lni-link"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-4 col-sm-6 planning-3">
                    <div class="single-portfolio mt-30">
                        <div class="portfolio-image">
                            <img src="{{ asset('images/portfolio/portfolio-3/6.png') }}" alt="">
                            <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                <div class="portfolio-content">
                                    <div class="portfolio-icon">
                                        <a class="image-popup-three" href="{{ asset('images/portfolio/portfolio-3/6.png') }}"><i class="lni-zoom-in"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                    <div class="portfolio-icon">
                                        <a href="#"><i class="lni-link"></i></a>
                                        <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portfolio-text">
                            <h4 class="portfolio-title"><a href="#">Graphics Design</a></h4>
                            <p class="text">Short description for the ones who look for something new. Awesome!</p>
                        </div>
                    </div> 
                </div>
            </div> 
        </div> 
    </section>
    
    
    <section class="portfolio-area portfolio-four pb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3">
                    <div class="portfolio-menu-4 text-center mt-50">
                        <ul>
                            <li data-filter="*" class="active">ALL WORK</li>
                            <li data-filter=".branding-4">BRANDING</li>
                            <li data-filter=".marketing-4">MARKETING</li>
                            <li data-filter=".planning-4">PLANNING</li>
                            <li data-filter=".research-4">RESEARCH</li>
                        </ul>
                    </div> 
                </div>
                <div class="col-lg-9 col-md-9">
                    <div class="row no-gutters grid-4 mt-50">
                        <div class="col-lg-4 col-sm-6 branding-4 planning-4">
                            <div class="single-portfolio">
                                <div class="portfolio-image">
                                    <img src="{{ asset('images/portfolio/portfolio-4/1.png') }}" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup-four" href="{{ asset('images/portfolio/portfolio-4/1.png') }}"><i class="lni-zoom-in"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                            <div class="portfolio-icon">
                                                <a href="#"><i class="lni-link"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-lg-4 col-sm-6 marketing-4 research-4">
                            <div class="single-portfolio">
                                <div class="portfolio-image">
                                    <img src="{{ asset('images/portfolio/portfolio-4/2.png') }}" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup-four" href="{{ asset('images/portfolio/portfolio-4/2.png') }}"><i class="lni-zoom-in"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                            <div class="portfolio-icon">
                                                <a href="#"><i class="lni-link"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-lg-4 col-sm-6 branding-4 marketing-4">
                            <div class="single-portfolio">
                                <div class="portfolio-image">
                                    <img src="{{ asset('images/portfolio/portfolio-4/3.png') }}" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup-four" href="{{ asset('images/portfolio/portfolio-4/3.png') }}"><i class="lni-zoom-in"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                            <div class="portfolio-icon">
                                                <a href="#"><i class="lni-link"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-lg-4 col-sm-6 planning-4 research-4">
                            <div class="single-portfolio">
                                <div class="portfolio-image">
                                    <img src="{{ asset('images/portfolio/portfolio-4/4.png') }}" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup-four" href="{{ asset('images/portfolio/portfolio-4/4.png') }}"><i class="lni-zoom-in"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                            <div class="portfolio-icon">
                                                <a href="#"><i class="lni-link"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-lg-4 col-sm-6 marketing-4">
                            <div class="single-portfolio">
                                <div class="portfolio-image">
                                    <img src="{{ asset('images/portfolio/portfolio-4/5.png') }}" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup-four" href="{{ asset('images/portfolio/portfolio-4/5.png') }}"><i class="lni-zoom-in"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                            <div class="portfolio-icon">
                                                <a href="#"><i class="lni-link"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-lg-4 col-sm-6 planning-4">
                            <div class="single-portfolio">
                                <div class="portfolio-image">
                                    <img src="{{ asset('images/portfolio/portfolio-4/6.png') }}" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup-four" href="{{ asset('images/portfolio/portfolio-4/6.png') }}"><i class="lni-zoom-in"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                            <div class="portfolio-icon">
                                                <a href="#"><i class="lni-link"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-lg-4 col-sm-6 research-4">
                            <div class="single-portfolio">
                                <div class="portfolio-image">
                                    <img src="{{ asset('images/portfolio/portfolio-4/7.png') }}" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup-four" href="{{ asset('images/portfolio/portfolio-4/7.png') }}"><i class="lni-zoom-in"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                            <div class="portfolio-icon">
                                                <a href="#"><i class="lni-link"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-lg-4 col-sm-6 branding-4 planning-4">
                            <div class="single-portfolio">
                                <div class="portfolio-image">
                                    <img src="{{ asset('images/portfolio/portfolio-4/8.png') }}" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup-four" href="{{ asset('images/portfolio/portfolio-4/8.png') }}"><i class="lni-zoom-in"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                            <div class="portfolio-icon">
                                                <a href="#"><i class="lni-link"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                        <div class="col-lg-4 col-sm-6 marketing-4">
                            <div class="single-portfolio">
                                <div class="portfolio-image">
                                    <img src="{{ asset('images/portfolio/portfolio-4/9.png') }}" alt="">
                                    <div class="portfolio-overlay d-flex align-items-center justify-content-center">
                                        <div class="portfolio-content">
                                            <div class="portfolio-icon">
                                                <a class="image-popup-four" href="{{ asset('images/portfolio/portfolio-4/9.png') }}"><i class="lni-zoom-in"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-3/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                            <div class="portfolio-icon">
                                                <a href="#"><i class="lni-link"></i></a>
                                                <img src="{{ asset('images/portfolio/portfolio-2/shape.svg') }}" alt="shape" class="shape">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> 
                        </div>
                    </div> 
                </div>
            </div> 
        </div> 
    </section>
    
    
    <section class="image-gallery-area image-gallery-one mb-100">
        <div class="container-fluid">
            <div class="row no-gutters">
                <div class="col-lg-3 col-sm-6">
                    <div class="single-image-gallery">
                        <div class="image">
                            <img src="{{ asset('images/portfolio/gallery-1/gallery-1.jpg') }}" alt="Gallery">
                        </div>
                        <div class="gallery-icon">
                            <a class="image-popup-five" href="{{ asset('images/portfolio/gallery-1/gallery-1.jpg') }}"><i class="lni-plus"></i></a>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-image-gallery">
                        <div class="image">
                            <img src="{{ asset('images/portfolio/gallery-1/gallery-2.jpg') }}" alt="Gallery">
                        </div>
                        <div class="gallery-icon">
                            <a class="image-popup-five" href="{{ asset('images/portfolio/gallery-1/gallery-2.jpg') }}"><i class="lni-plus"></i></a>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-image-gallery">
                        <div class="image">
                            <img src="{{ asset('images/portfolio/gallery-1/gallery-3.jpg') }}" alt="Gallery">
                        </div>
                        <div class="gallery-icon">
                            <a class="image-popup-five" href="{{ asset('images/portfolio/gallery-1/gallery-3.jpg') }}"><i class="lni-plus"></i></a>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-image-gallery">
                        <div class="image">
                            <img src="{{ asset('images/portfolio/gallery-1/gallery-4.jpg') }}" alt="Gallery">
                        </div>
                        <div class="gallery-icon">
                            <a class="image-popup-five" href="{{ asset('images/portfolio/gallery-1/gallery-4.jpg') }}"><i class="lni-plus"></i></a>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-image-gallery">
                        <div class="image">
                            <img src="{{ asset('images/portfolio/gallery-1/gallery-4.jpg') }}" alt="Gallery">
                        </div>
                        <div class="gallery-icon">
                            <a class="image-popup-five" href="{{ asset('images/portfolio/gallery-1/gallery-4.jpg') }}"><i class="lni-plus"></i></a>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-image-gallery">
                        <div class="image">
                            <img src="{{ asset('images/portfolio/gallery-1/gallery-3.jpg') }}" alt="Gallery">
                        </div>
                        <div class="gallery-icon">
                            <a class="image-popup-five" href="{{ asset('images/portfolio/gallery-1/gallery-3.jpg') }}"><i class="lni-plus"></i></a>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-image-gallery">
                        <div class="image">
                            <img src="{{ asset('images/portfolio/gallery-1/gallery-2.jpg') }}" alt="Gallery">
                        </div>
                        <div class="gallery-icon">
                            <a class="image-popup-five" href="{{ asset('images/portfolio/gallery-1/gallery-2.jpg') }}"><i class="lni-plus"></i></a>
                        </div>
                    </div> 
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="single-image-gallery">
                        <div class="image">
                            <img src="{{ asset('images/portfolio/gallery-1/gallery-1.jpg') }}" alt="Gallery">
                        </div>
                        <div class="gallery-icon">
                            <a class="image-popup-five" href="{{ asset('images/portfolio/gallery-1/gallery-1.jpg') }}"><i class="lni-plus"></i></a>
                        </div>
                    </div> 
                </div>
            </div> 
        </div> 
    </section>
    
    
    
    
    
    
    <script src="{{ asset('js/modernizr-3.6.0.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/jquery-1.12.4.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/popper.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/slick.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/isotope.pkgd.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/imagesloaded.pkgd.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/jquery.magnific-popup.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/jquery.appear.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/waypoints.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/jquery.counterup.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/validator.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/jquery.nice-select.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/jquery.countdown.min.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="{{ asset('js/main.js') }}" type="9505328eff733e2d5d993dcf-text/javascript"></script>
    <script src="https://ajax.cloudflare.com/cdn-cgi/scripts/95c75768/cloudflare-static/rocket-loader.min.js" data-cf-settings="9505328eff733e2d5d993dcf-|49" defer=""></script></body>
    </html>
    