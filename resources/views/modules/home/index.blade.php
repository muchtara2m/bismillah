@extends('layouts.master')
@section('title')
Aybis semangART
@endsection
@section('titleContent')
Home
@endsection
@section('content')
<div class="be-content">
    <div class="main-content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="widget widget-fullwidth be-loading">
                    <div class="widget-head">
                        <div class="tools">
                            <div class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown"><span class="icon mdi mdi-more-vert d-inline-block d-md-none"></span></a>
                                <div class="dropdown-menu" role="menu"><a class="dropdown-item" href="#">Week</a><a class="dropdown-item" href="#">Month</a><a class="dropdown-item" href="#">Year</a>
                                    <div class="dropdown-divider"></div><a class="dropdown-item" href="#">Today</a>
                                </div>
                            </div><span class="icon mdi mdi-chevron-down"></span><span class="icon toggle-loading mdi mdi-refresh-sync"></span><span class="icon mdi mdi-close"></span>
                        </div>
                        <div class="button-toolbar d-none d-md-block">
                            <div class="btn-group">
                                <button class="btn btn-secondary" type="button">Week</button>
                                <button class="btn btn-secondary active" type="button">Month</button>
                                <button class="btn btn-secondary" type="button">Year</button>
                            </div>
                            <div class="btn-group">
                                <button class="btn btn-secondary" type="button">Today</button>
                            </div>
                        </div><span class="title">Recent Movement</span>
                    </div>
                    <div class="widget-chart-container">
                        <div class="widget-chart-info">
                            <ul class="chart-legend-horizontal">
                                <li><span data-color="main-chart-color1" style="background-color: rgb(66, 133, 244);"></span> Purchases</li>
                                <li><span data-color="main-chart-color2" style="background-color: rgb(129, 173, 248);"></span> Plans</li>
                                <li><span data-color="main-chart-color3" style="background-color: rgb(162, 195, 250);"></span> Services</li>
                            </ul>
                        </div>
                        <div class="widget-counter-group widget-counter-group-right">
                            <div class="counter counter-big">
                                <div class="value">25%</div>
                                <div class="desc">Purchase</div>
                            </div>
                            <div class="counter counter-big">
                                <div class="value">5%</div>
                                <div class="desc">Plans</div>
                            </div>
                            <div class="counter counter-big">
                                <div class="value">5%</div>
                                <div class="desc">Services</div>
                            </div>
                        </div>
                        <div id="main-chart" style="height: 260px; padding: 0px; position: relative;">
                            <canvas class="flot-base" width="2800" height="520" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1400px; height: 260px;"></canvas>
                            <canvas class="flot-overlay" width="2800" height="520" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 1400px; height: 260px;"></canvas>
                        </div>
                    </div>
                    <div class="be-spinner">
                        <svg width="40px" height="40px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
                            <circle class="circle" fill="none" stroke-width="4" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
                        </svg>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    
    var bulan = $('bulan');
    var tahun = $('tahun');
    month = [
    'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember'
    ];
    month.forEach(element => {
        // console.log(element);    
    });
</script>
@endsection
