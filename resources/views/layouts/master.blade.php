<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/img/logo-fav.png">
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/perfect-scrollbar/css/perfect-scrollbar.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/material-design-icons/css/material-design-iconic-font.min.css')}}"/>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jquery.vectormap/jquery-jvectormap-1.2.2.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/jqvmap/jqvmap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/lib/datetimepicker/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}" type="text/css"/>
    @yield('stylesheet')
    @yield('customcss')
  </head>
  <body>
    <div class="be-wrapper be-fixed-sidebar">
        @include('layouts.include.header')
        @include('layouts.include.sidebar')
        @yield('content')

        @include('layouts.include.right-sidebar')
    </div>
    <script src="{{ asset('assets/lib/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/lib/perfect-scrollbar/js/perfect-scrollbar.jquery.min.js') }}"></script>
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/lib/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/lib/jquery-flot/jquery.flot.j') }}s"></script>
    <script src="{{ asset('assets/lib/jquery-flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/lib/jquery-flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/lib/jquery-flot/plugins/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('assets/lib/jquery-flot/plugins/curvedLines.js') }}"></script>
    <script src="{{ asset('assets/lib/jquery.sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/lib/countup/countUp.min.js') }}"></script>
    <script src="{{ asset('assets/lib/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/lib/jqvmap/jquery.vmap.min.js') }}"></script>
    <script src="{{ asset('assets/lib/jqvmap/maps/jquery.vmap.world.js') }}"></script>  
    <script type="text/javascript">
        $(document).ready(function(){
            //initialize the javascript
            App.init();
            // App.dashboard();
        });
      </script>

      @yield('scripts')
  </body>
</html>
